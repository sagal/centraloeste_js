import { FTPClient } from "https://deno.land/x/ftpc@v1.2.1/mod.ts";

const clientId = Deno.env.get('CLIENT_ID')
const clientIntegrationId = Deno.env.get('CLIENT_INTEGRATION_ID')
const clientEcommerce = JSON.parse(Deno.env.get('CLIENT_ECOMMERCE'))

const ftpHost = Deno.env.get('FTP_HOST')
const user = Deno.env.get('FTP_USER')
const pass = Deno.env.get('FTP_PASS')
const partial = Deno.env.get('PARTIAL')

function createPayload(articlesMap, sku) {
    articlesMap[sku] = {
        sku: sku,
        client_id: clientId,
        integration_id: clientIntegrationId,
        options: {
            merge: false,
            partial: partial === "true"
        },
        ecommerce: Object.values(clientEcommerce).map(ecommerce_id => {
            return {
                ecommerce_id: ecommerce_id,
                variants: [],
                properties: []
            }
        })
    };
}

const init = async () => {

    const decoder = new TextDecoder();
    const articlesMap = {};

    let client = new FTPClient(ftpHost, {
        user: user,
        pass: pass,
        mode: "passive",
        port: 21,
    });


    await client.connect();
    let stock_data = await downloadFile(client, "./Subida/Interfaces/lista_stock_.csv")
    let price_data = await downloadFile(client, "./Subida/Interfaces/lista_price_.csv")

    if (stock_data) {
        await client.rename("/Subida/Interfaces/lista_stock_.csv", "/Subida/Interfaces/procesados/lista_stock_.csv");
        let stock_data_text = decoder.decode(stock_data).split("\n")

        for (let i = 1; i < stock_data_text.length; i++) {
            let line = stock_data_text[i]
            if (line) {
                let data = line.split(";")
                let sku = data[0]
                let stock = data[1]
                if (sku && sku != '') {
                    if (!articlesMap[sku]) {
                        createPayload(articlesMap, sku);
                    }
                    for (let element of articlesMap[sku].ecommerce) {
                        element.properties.push({ "stock": parseInt(stock) })
                    }
                }
            }
        }
    }

    if (price_data) {
        await client.rename("/Subida/Interfaces/lista_price_.csv", "/Subida/Interfaces/procesados/lista_price_.csv");
        let price_data_text = decoder.decode(price_data).split("\n")

        for (let i = 1; i < price_data_text.length; i++) {
            let line = price_data_text[i]
            if (line) {
                let data = line.split(";")
                let sku = data[0]
                let price = data[1]
                if (sku && sku != '') {
                    if (!articlesMap[sku]) {
                        createPayload(articlesMap, sku);
                    }
                    for (let element of articlesMap[sku].ecommerce) {
                        element.properties.push({
                            "price": {
                                "value": Math.ceil(parseFloat(price)* 1.05),
                                "currency": "$"
                            }
                        })
                    }
                }
            }
        }
    }

    for (const [key, value] of Object.entries(articlesMap)) {
        try {
            await sagalDispatch(value)
        } catch (e) {
            console.log(`ERROR: ${e}`)
        }
    }
    await client.close();
}

async function downloadFile(client, file) {
    try {
        let data = await client.download(file);
        return data
    } catch (e) {
        return null
    }
}

await init();
