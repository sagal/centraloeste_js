import { FTPClient } from "https://deno.land/x/ftpc@v1.2.1/mod.ts";
import axiod from "https://deno.land/x/axiod/mod.ts";


const ftpHost = Deno.env.get('FTP_HOST')
const user = Deno.env.get('FTP_USER')
const pass = Deno.env.get('FTP_PASS')

let errorLines = []
let processedLines = []

const init = async () => {

    const decoder = new TextDecoder();
    const encoder = new TextEncoder();

    let client = new FTPClient(ftpHost, {
        user: user,
        pass: pass,
        mode: "passive",
        port: 21,
    });

    await client.connect();
    let list = await client.list('/Subida/Estados');
    list = list.filter(str => str.includes('.csv'))
    let time = new Date().getTime();
    for (let element of list) {
        let data = await downloadFile(client, element)
        let lines = decoder.decode(data).split("\n")
        for (let line of lines) {
            try {
                if (line) {
                    let tokens = line.split(';')
                    let order = Number(tokens[0])
                    await notifyOrder(order)
                    processedLines.push(line)
                }
            } catch (e) {
                console.log(`An error ocurred notifing order: ${e.message}`)
                errorLines.push(`${line};${e.message.replaceAll('\n', "")}`)
            }
        }
        await client.rename(element, `/Subida/Estados/procesados/order_status_${time}.csv`)
    }

    await uploadProcessedLines(client, encoder, time);
    await uploadErrorLines(client, encoder, time);
    await client.close();
}

async function downloadFile(client, element) {
    try {
        let data = await client.download(element);
        return data
    } catch (e) {
        throw new Error(`Could not download: ${element}`)
    }
}


async function notifyOrder(order) {
    console.log(`Sending status for order ${order}...`)
    try{
        await axiod.put(`https://app.sagal.io/api/purchases/${order}/deliver`, {}, {
            headers: {
                "Content-Type": "application/json",
            },
            timeout: 500000
        })
        console.log(`Status sent for order ${order}!`)
    }catch(e){
        throw new Error(`Could not change state on sagal for ${order}!`)
    }
}

async function uploadProcessedLines(client, encoder, time) {
    if (processedLines.length) {
        let lines = processedLines.join("\n")
        let uploadData = await encoder.encode(lines)
        await client.upload(`/Subida/Estados/procesados_exito/order_status_${time}.csv`, uploadData)
    }

}

async function uploadErrorLines(client, encoder, time) {
    if (errorLines.length) {
        let lines = errorLines.join("\n")
        let uploadData = await encoder.encode(lines)
        await client.upload(`/Subida/Estados/procesados_error/order_status_${time}.csv`, uploadData)
    }
}

await init();