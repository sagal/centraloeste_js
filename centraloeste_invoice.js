import { FTPClient } from "https://deno.land/x/ftpc@v1.2.1/mod.ts";
import { S3Bucket } from "https://deno.land/x/s3@0.4.1/mod.ts";
import axiod from "https://deno.land/x/axiod/mod.ts";


const ftpHost = Deno.env.get('FTP_HOST')
const user = Deno.env.get('FTP_USER')
const pass = Deno.env.get('FTP_PASS')
const awsKeyId = Deno.env.get("AWS_ACCESS_KEY_ID")
const awsSecretKey = Deno.env.get("AWS_SECRET_ACCESS_KEY")
const sagalUrl = "https://app.sagal.io"


const bucket = new S3Bucket({
    accessKeyID: awsKeyId,
    secretKey: awsSecretKey,
    bucket: "sagal-excel-uploads-0181239103940129",
    region: "us-east-1"
});


const init = async () => {
    try {
        let client = new FTPClient(ftpHost, {
            user: user,
            pass: pass,
            mode: "passive",
            port: 21,
        });

        await client.connect();
        let list = await client.list('/Subida/Pedidos');
        let filter = /factura_(?<idSagal>\d+).pdf/
        list = list.filter(str => str.match(filter))
        for (let element of list) {
            let name = element.split("/").pop()
            console.log(`Invoice name is: ${name}`)
            let match = element.match(filter)
            let idSagal = Number(match.groups.idSagal)
            try {
                let pdf = await client.download(element);
                let result = await uploadFile(element, pdf)
                await uploadInvoiceToSagal(idSagal, result)
                await client.rename(element, `/Subida/Pedidos/procesados/${name}`);
            } catch (e) {
                console.log(e.message)
                await client.rename(element, `/Subida/Pedidos/procesados_error/${name}`);
            }
        }
        await client.close();
    } catch (e) {
        console.log(`An error ocurred: ${e.message}`)
        throw new Error(e.message)
    }
}

async function uploadInvoiceToSagal(idSagal, url) {
    let sagalInvoiceUrl = `${sagalUrl}/api/purchases/${idSagal}/invoices`
    await axiod.post(sagalInvoiceUrl, { url: url }, {
        headers: {
            "Content-Type": "application/json",
        },
        timeout: 500000
    })
    console.log(`Invoice sent for order ${idSagal}!`)
}

const uploadFile = async (filename, file) => {
    const filePath = `centraloeste_invoice/${filename}`
    await bucket.putObject(`/${filePath}`, file, {
        contentType: "application/pdf",
        acl: 'public-read'
    })
    let uploadedUrl = `https://sagal-excel-uploads-0181239103940129.s3.amazonaws.com/${filePath}`
    console.log(`Invoice uploaded: ${uploadedUrl}`)
    return uploadedUrl
}


await init();